#define _POSIX_C_SOURCE 200809L

#include <emscripten.h>

#include <mandelbrot-numerics.h>

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <mpfr.h>

static inline bool arg_precision(const char *arg, bool *native, int *bits) {
  if (0 == strcmp("double", arg)) {
    *native = true;
    *bits = 53;
    return true;
  } else {
    char *check = 0;
    errno = 0;
    long int li = strtol(arg, &check, 10);
    bool valid = ! errno && arg != check && ! *check;
    int i = li;
    if (valid && i > 1) {
      *native = false;
      *bits = i;
      return true;
    }
  }
  return false;
}

static inline bool arg_double(const char *arg, double *x) {
  char *check = 0;
  errno = 0;
  double d = strtod(arg, &check);
  if (! errno && arg != check && ! *check) {
    *x = d;
    return true;
  }
  return false;
}

static inline bool arg_int(const char *arg, int *x) {
  char *check = 0;
  errno = 0;
  long int li = strtol(arg, &check, 10);
  if (! errno && arg != check && ! *check) {
    *x = li;
    return true;
  }
  return false;
}

static inline bool arg_rational(const char *arg, mpq_t x) {
  int ok = mpq_set_str(x, arg, 10);
  mpq_canonicalize(x);
  return ok == 0;
}

static inline bool arg_mpfr(const char *arg, mpfr_t x) {
  return 0 == mpfr_set_str(x, arg, 10, MPFR_RNDN);
}

static inline bool arg_mpc(const char *re, const char *im, mpc_t x) {
  int ok
    = mpfr_set_str(mpc_realref(x), re, 10, MPFR_RNDN)
    + mpfr_set_str(mpc_imagref(x), im, 10, MPFR_RNDN);
  return ok == 0;
}

static const double twopi = 6.283185307179586;

#define BOOK "https://mathr.co.uk/mandelbrot/book-draft/"

extern void mandelbrot_numerics(char *in, int in_size)
{
#define FAIL do{char *s=strdup(usage);emscripten_worker_respond(s,strlen(s)+1);free(s);return;}while(0)

  // split arguments
  int argc = 0;
  for (int i = 0; i < in_size; ++i)
  {
    argc += (in[i] == 0);
  }
  if (! (argc > 0)) { emscripten_worker_respond(0,0); return; }
  char *argv[argc];
  char *arg = in;
  int j = 0;
  for (int i = 0; i < in_size; ++i)
  {
    if (in[i] == 0)
    {
      argv[j++] = arg;
      arg = &in[i + 1];
    }
  }
  if (j != argc) { emscripten_worker_respond(0,0); return; }

  if (! strcmp("nucleus", argv[0])) {
    const char *usage =
      "<h1><a href='#' title='mandelbrot-numerics'>mandelbrot-numerics</a></h1>\n"
      "<h2><a href='#nucleus' title='nucleus'>nucleus</a></h2>\n"
      "<p><a href='" BOOK "#nucleus' title='nucleus'>nucleus in mandelbook (draft)</a></p>\n"
      "<h3>usage</h3>\n"
      "<dl>\n"
      "  <dt>precision</dt><dd>can be either 'double' or a number of bits</dd>\n"
      "  <dt>cre</dt><dd>floating point real coordinate of the complex 'c' starting guess</dd>\n"
      "  <dt>cim</dt><dd>floating point imaginary coordinate of the complex 'c' starting guess</dd>\n"
      "  <dt>period</dt><dd>integer period of the target hyperbolic component</dd>\n"
      "  <dt>maxsteps</dt><dd>maximum number of steps of Newton's method to perform</dd>\n"
      "</dl>\n"
      "<h3>examples</h3>\n"
      "<ul>\n"
      "  <li><a href='#nucleus double 0 1 10 16' title='#nucleus double 0 1 10 16'>#nucleus double 0 1 10 16</a></li>\n"
      "  <li><a href='#nucleus 1000 -2 0 250 16' title='#nucleus 1000 -2 0 250 16'>#nucleus 1000 -2 0 250 16</a></li>\n"
      "</ul>\n"
      ;
    if (argc != 6) FAIL;
    bool native = true;
    int bits = 0;
    if (! arg_precision(argv[1], &native, &bits)) FAIL;
    if (native) {
      double cre = 0;
      double cim = 0;
      int period = 0;
      int maxsteps = 0;
      if (! arg_double(argv[2], &cre)) FAIL;
      if (! arg_double(argv[3], &cim)) FAIL;
      if (! arg_int(argv[4], &period)) FAIL;
      if (! arg_int(argv[5], &maxsteps)) FAIL;
      double _Complex c = 0;
      m_d_nucleus(&c, cre + I * cim, period, maxsteps);
      const char *result =
        "<h1><a href='#' title='mandelbrot-numerics'>mandelbrot-numerics</a></h1>\n"
        "<h2><a href='#nucleus' title='nucleus'>nucleus</a></h2>\n"
        "<h3>input</h3>\n<p id='input'><a href='#%s %s %s %s %s %s' title='#%s %s %s %s %s %s'>#%s %s %s %s %s %s</a></p>\n"
        "<h3>output</h3>\n<textarea id='output'>%.16e %.16e</textarea>\n"
        ;
      int out_bytes = strlen(result) + 3 * in_size + 100;
      char *out = malloc(out_bytes);
      snprintf
        ( out, out_bytes
        , result
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , creal(c), cimag(c)
        );
      int out_size = strnlen(out, out_bytes) + 1;
      emscripten_worker_respond(out, out_size);
      free(out);
      return;
    } else {
      mpc_t c_guess;
      int period = 0;
      int maxsteps = 0;
      mpc_init2(c_guess, bits);
      if (! arg_mpc(argv[2], argv[3], c_guess)) FAIL;
      if (! arg_int(argv[4], &period)) FAIL;
      if (! arg_int(argv[5], &maxsteps)) FAIL;
      mpc_t c_out;
      mpc_init2(c_out, bits);
      m_r_nucleus(c_out, c_guess, period, maxsteps, 0);
      const char *result =
        "<h1><a href='#' title='mandelbrot-numerics'>mandelbrot-numerics</a></h1>\n"
        "<h2><a href='#nucleus' title='nucleus'>nucleus</a></h2>\n"
        "<h3>input</h3>\n<p id='input'><a href='#%s %s %s %s %s %s' title='#%s %s %s %s %s %s'>#%s %s %s %s %s %s</a></p>\n"
        "<h3>output</h3>\n<textarea id='output'>%Re %Re</textarea>\n"
        ;
      int out_size = mpfr_snprintf
        ( 0, 0
        , result
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , mpc_realref(c_out), mpc_imagref(c_out)
        ) + 1;
      char *out = malloc(out_size);
      mpfr_snprintf
        ( out, out_size
        , result
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]
        , mpc_realref(c_out), mpc_imagref(c_out)
        );
      emscripten_worker_respond(out, out_size);
      free(out);
      mpc_clear(c_out);
      mpc_clear(c_guess);
      return;
    }
  }

  const char *usage =
    "<h1><a href='#' title='mandelbrot-numerics'>mandelbrot-numerics</a></h1>\n"
    "<h2>available commands</h2>\n"
    "<h3><a href='#nucleus' title='nucleus'>nucleus</a></h3>\n"
    ;
  FAIL;
#undef FAIL
}
