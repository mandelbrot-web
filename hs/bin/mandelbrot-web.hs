{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
import Prelude hiding (Rational)
import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)
import Data.Bits (bit)
import Data.Char (ord)
import Data.List (nub)
import Data.Maybe (isJust)
import Data.Text (unpack)
import Data.Text.Encoding (decodeUtf8')
import Data.Text.Lazy (pack)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Text.Parsec
  ( choice
  , try
  , getPosition
  , manyTill
  , anyChar
  , eof
  , setPosition
  , unexpected
  , runParser
  )
import Control.Monad.Writer
  ( execWriter
  , tell
  )
import Data.Maybe
  ( isNothing
  )

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , BinaryAngle(..)
  , Block(..)
  , ExternalAngle(..)
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Rational
  , Periods(..)
  , (!)
  , kneading
  , associated
  , rational
  , binary
  , binaryAngle
  , compact
  , addressAngles
  , externalAngles
  , denominator
  , zero
  , one
  , preperiod
  , period
  , safePeriods
  , periods
  , internalAddress
  , angledAddress
  )
import Mandelbrot.Text
  ( Parser
  , Parse(parser)
  , Plain(plain)
  , HTML(html)
  , Glyph(ArrowRight, Star)
  )

data Item
  = BAn BinaryAngle
  | Knd Kneading
  | AAd AngledAddress
  | IAd InternalAddress
  | Rat Rational
  | Per Periods
  | IAn InternalAngle
  | EAn ExternalAngle

item :: Parser Item
item = choice
  [ t (BAn `fmap` parser) -- starts with ., ends with )
  , t (Knd `fmap` parser) -- starts with 0, 1, *, (, ends with )
  , t (Rat `fmap` parser) -- contains over
  , t (Per `fmap` parser) -- contains p
  , t (AAd `fmap` parser) -- possibly contains _, over, arrowright
  , t (IAd `fmap` parser) -- possibly contains arrowright, no over
  , do
      p <- getPosition
      s <- manyTill anyChar eof
      setPosition p
      unexpected . show . abbreviate 50 $ s
  ]
  where
    t p = try (do r <- p ; eof ; return r)
    abbreviate n s = case map (splitAt n) (lines s) of
      [(s', [])] -> s'
      (s', _):_ -> s' ++ "..."
      _ -> ""


class Valid t where
  valid :: t -> Bool

instance Valid AngledAddress where
  valid (Unangled 1) = True
  valid (Angled 1 r' a') = go 1 r' a'
    where
      go p r (Unangled pp) =
        zero < r && r < one && p < pp && pp <= p * fromInteger (denominator r)
      go p r (Angled pp r1 a1) =
        zero < r && r < one && p < pp && pp <= p * fromInteger (denominator r) &&
        go pp r1 a1
  valid _ = False

realizable :: AngledAddress -> Bool
realizable a = valid a && all (isJust . addressAngles) (truncations a)

truncations :: AngledAddress -> [AngledAddress]
truncations a' = execWriter $ go (Unangled 1) a'
  where
    go a0 (Unangled _) = tell [a0]
    go a0 (Angled p r (Unangled pp)) = tell [a0,a1,a2]
      where
        a1 = appendAddress a0 r (p * fromInteger (denominator r))
        a2 = appendAddress a0 r pp
    go a0 (Angled p r a3@(Angled pp _ _)) = tell [a0,a1] >> go a2 a3
      where
        a1 = appendAddress a0 r (p * fromInteger (denominator r))
        a2 = appendAddress a0 r pp

class Plain t => Hyper t where
  hyper :: t -> String
  link :: t -> String -> String
  link t s = link' t (stringToHtmlString s)
  link' :: t -> String -> String
  link' t s = "<a href='?q=" ++ q ++ "' title='" ++ q ++ "'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain t)

class Page t where
  page :: t -> String

instance Page AngledAddress where
  page a0
    | realizable a0 = unlines $
      [ "<table>" ] ++
      map tr (reverse (nub (truncations a0))) ++
      [ "</table>" ]
    | otherwise = ""
    where
      tr a = "<tr><td>" ++ hyper a ++ "</td><td><span class='m-stack'><span>" ++ hyper (binary lo) ++ "</span><span>" ++ hyper (binary hi) ++ "</span></span></td></tr>"
        where
          Just (lo, hi) = addressAngles a

instance Page BinaryAngle where
  page b = unlines $
    [ "<h4>Binary</h4>"
    , link' b (html b)
    , "<h4>Rational</h4>"
    , link' r (html r)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods b))
    , "<h4>Kneading</h4>"
    , link' k (html k)
    , case i' of Just i -> "<h4>Internal Address</h4>\n" ++ link' i (html i) ; Nothing -> ""
    , case a' of Just a -> "<h4>Angled Address</h4>\n" ++ link' a (html a) ; Nothing -> ""
    ] ++ if null (drop 1 es) then [] else
          [ "<h4>Landings</h4>"
          , "<table>" ] ++
          map tr es ++
          [ "</table>" ]
    where
      r = rational b
      k = kneading r
      i' = internalAddress k
      a' = angledAddress r
      es = externalAngles b
      tr e = "<tr" ++ (if b == e then " class='m-strong'" else "") ++ "><td>" ++ hyper e ++ "</td><td>" ++ hyper (rational e) ++ "</td></tr>"

instance Page InternalAngle where
  page i = unlines $
    [ "<h4>Rational</h4>"
    , link' i (html i)
    , "<h4>Bulb</h4>"
    , "<table>" ] ++
    map tr [lo, hi] ++
    [ "</table>"
{-
    , "<h4>Spokes</h4>"
    , "<table>"
    , map tr spokes
    , "</table>"
-}
    ]
    where
      Just (lo, hi) = addressAngles (Angled 1 i (Unangled (fromInteger (denominator i))))
      tr e = "<tr><td>" ++ hyper (binary e) ++ "</td><td>" ++ hyper e ++ "</td></tr>"

instance Page Kneading where
  page k@(StarPeriodic _) = unlines $
    [ "<h4>" ++ html Star ++ "-Periodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ] ++ case associated k of
      Just (u, l) ->
        [ "<h4>Upper</h4>"
        , link' u (html u)
        , "<h4>Lower</h4>"
        , link' l (html l)
        ]
      _ -> []
  page k@(Periodic _) = unlines $
    [ "<h4>Periodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ]
  page k@(PrePeriodic _ _) = unlines $
    [ "<h4>Preperiodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ]

instance Page Periods where
  page (Periods (pp, p)) = unlines $
    [ "<h4>Binary Angles</h4>"
    , "<table>" ] ++
    [ "<tr><td>" ++ hyper (BinaryAngle bb b) ++ "</td></tr>"
    | ii <- [ 0 .. bit pp - 1 ], let bb = Block ii pp
    , i <- [ 0 .. bit p - 1 ], let b = Block i p
    , binaryAngle bb b == BinaryAngle bb b
    ] ++ [ "</table>" ] ++
    (if pp == 0 then
      [ "<h4>" ++ html Star ++ "-Periodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (StarPeriodic b) ++ "</td></tr>"
      | i <- [ 0 .. bit (p - 1) - 1 ], let b = Block i (p - 1), b ! 0
      ] ++ [ "</table>"
      , "<h4>Periodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (Periodic b) ++ "</td></tr>"
      | i <- [ 0 .. bit p - 1 ], let b = Block i p, b ! 0, b == compact b
      ] ++ [ "</table>" ]
    else
      [ "<h4>Preperiodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (PrePeriodic bb b) ++ "</td></tr>"
      | ii <- [ 0 .. bit pp - 1 ], let bb = Block ii pp, bb ! 0
      , i <- [ 0 .. bit p - 1 ], let b = Block i p
      , binaryAngle bb b == BinaryAngle bb b -- fixme need generalized version
      ] ++ [ "</table>" ])

instance Hyper AngledAddress where
  hyper a
    | realizable a = "<span class='m-angledaddress'>" ++ hyper' (Unangled 1) a ++ "</span>"
    | otherwise = "<span class='m-angledaddress'>" ++ stringToHtmlString (plain a) ++ "</span>"
    where
    hyper' a0 (Unangled p) = link a0 . plain $ p
    hyper' a0 (Angled p r (Unangled pp))
      =  link a0 (plain p)
      ++ "<sub>" ++ link' r (html r) ++ "</sub>" ++ html ArrowRight
      ++ link a2 (plain pp)
      where
        a2 = appendAddress a0 r pp
    hyper' a0 (Angled p r a3@(Angled pp _ _))
      =  link a0 (plain p)
      ++ "<sub>" ++ link' r (html r) ++ "</sub>" ++ html ArrowRight
      ++ hyper' a2 a3
      where
        a2 = appendAddress a0 r pp

instance Hyper InternalAddress where
  hyper (InternalAddress (1:a)) = "<span class='m-internaladdress'>" ++ hyper' (InternalAddress [1]) a ++ "</span>"
    where
      hyper' a0 [] = link a0 . plain . period $ a0
      hyper' a0 (p:ps) =
        (link a0 . plain . period $ a0) ++
        html ArrowRight ++
        hyper' (appendInternal a0 p) ps
  hyper i = "<span class='m-internaladdress'>" ++ stringToHtmlString (plain i) ++ "</span>"

appendInternal :: InternalAddress -> Int -> InternalAddress
appendInternal (InternalAddress ps) p = InternalAddress (ps ++ [p])

instance Hyper InternalAngle where
  hyper i = link' i (html i)
  link' i s = "<a href='?q=" ++ q ++ "&amp;t=internal-angle' title='" ++ q ++ " (internal angle)'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain i)
instance Hyper ExternalAngle where
  hyper e = link' e (html e)
  link' e s = "<a href='?q=" ++ q ++ "&amp;t=external-angle' title='" ++ q ++ " (external angle)'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain e)
instance Hyper BinaryAngle where hyper b = link' b (html b)
instance Hyper Kneading where hyper k = link' k (html k)
instance Hyper Periods where hyper p = link' p (html p)

appendAddress :: AngledAddress -> InternalAngle -> Int -> AngledAddress
appendAddress (Unangled p0) r p = Angled p0 r (Unangled p)
appendAddress (Angled p0 r0 a0) r p = Angled p0 r0 (appendAddress a0 r p)

maxPeriod :: Int
maxPeriod = 100

maxPeriodList :: Int
maxPeriodList = 7

describe :: Item -> Either String (String, String, String)
describe (BAn b)
  | preperiod b + period b > maxPeriod = Left "binary angle too large"
  | zero <= r && r <= one && binary r == b = Right (plain b, html b, "<h3>External Angle</h3>\n" ++ page b)
  | otherwise = Left "invalid binary angle"
  where r = rational b
describe (EAn r)
  | isNothing (safePeriods maxPeriod r) = Left "external angle too large"
  | zero <= r && r <= one = Right (plain r, html r, "<h3>External Angle</h3>\n" ++ page (binary r))
  | otherwise = Left "invalid external angle"
describe (IAn i)
  | denominator i > toInteger maxPeriod = Left "internal angle too large"
  | zero < i && i < one = Right (plain i, html i, "<h3>Internal Angle</h3>\n" ++ page i)
  | otherwise = Left "invalid internal angle"
describe (Knd k)
  | preperiod k + period k > maxPeriod = Left "kneading sequence too large"
  | otherwise = Right (plain k, html k, "<h3>Kneading Sequence</h3>\n" ++ page k)
describe (Rat r) = Right (plain r, html r, "<h3>Rational</h3>\n" ++ if zero <= r && r <= one then "<h4>External Angle</h4>\n" ++ link' e (html e) ++ "\n" ++ if zero < r && r < one then "<h4>Internal Angle</h4>\n" ++ link' i (html i) ++ "\n" else "" else "")
  where
    i = InternalAngle r
    e = ExternalAngle r
describe (AAd a)
  | period a > maxPeriod = Left "angled address too large"
  | realizable a = Right (plain a, html a, "<h3>Angled Address</h3>\n" ++ page a)
  | otherwise = Left "invalid angled address"
describe (IAd a)
  | period a > maxPeriod = Left "internal address too large"
  | otherwise = Right (plain a, html a, unlines $ [ plain a ])
describe (Per ps@(Periods (pp, p)))
  | pp + p > maxPeriodList = Left "periods too large"
  | pp >= 0 && p >= 1 = Right (plain ps, html ps, "<h3>Periods</h3>\n" ++ page ps)
  | otherwise = Left "invalid periods"

app :: Application
app request respond = do
  case lookup "q" (queryString request) of
    Just (Just b) -> case decodeUtf8' b of
      Right t -> do
        let s = unpack t
        case runParser item () "" s of
          Right i -> case describe (case i of
                Rat r -> case lookup "t" (queryString request) of
                  Just (Just "internal-angle")
                    | zero < r && r < one -> IAn (InternalAngle r)
                  Just (Just "external-angle")
                    | zero <= r && r <= one -> EAn (ExternalAngle r)
                  _ -> i
                _ -> i) of
            Left e -> err e
            Right (ptitle, title, content) ->
              respond $ responseLBS
                 status200
                 [("Content-Type", "text/html")]
                 (encodeUtf8 (pack ("<html><head>\n<title>" ++ stringToHtmlString ptitle ++ "</title>\n<style type='text/css'>\n" ++ unlines css ++ "</style></head><body><div>\n<h1>mandelbrot-web</h1>\n<h2>" ++ title ++ "</h2>\n" ++ content ++ "\n</div></body></html>")))
          Left e -> err $ "parse fail: " ++ show e
      Left e -> err $ "unicode fail: " ++ show e
    _ -> err $ "not found"
  where
    err s = respond $ responseLBS
              status404
              [("Content-Type", "text/plain")]
              (encodeUtf8 (pack s))

css :: [String]
css =
  [ "body { text-align: center; }"
  , "body > div { display: inline-block; margin: auto; }"
  , "body > div > dl { display: inline-block; margin: auto }"
  , "body > div > table { display: inline-block; margin: auto }"
  , "body > div > * > * { text-align: left; }"
  , "a { text-decoration: none; }"
  , "dt { font-weight: bold; }"
  , ".m-stack { display: inline-block; text-align: right; vertical-align: middle; }"
  , ".m-stack > * { display: block; }"
  , ".m-strong { font-weight: bold; }"
  , ".m-binaryangle, .m-kneading { font-family: monospace; }"
  , ".m-binaryangle .m-periodic, .m-kneading .m-periodic { border-top: 1px solid; }"
  , ".m-rational { display: inline-block; text-align: right; vertical-align: middle; font-size: 75%; }"
  , ".m-rational .m-over { display: none; }"
  , ".m-rational .m-denominator { display: block; border-top: 1px solid; }"
  , ".m-angledaddress .m-arrowright, .m-internaladdress .m-arrowright { margin-left: 5px; margin-right: 5px; }"
  ]


main :: IO ()
main = do
  putStrLn $ "http://localhost:8080/"
  run 8080 app

-- | Processing Strings into Html friendly things.
stringToHtmlString :: String -> String
stringToHtmlString = concatMap fixChar
    where
      fixChar '<' = "&lt;"
      fixChar '>' = "&gt;"
      fixChar '&' = "&amp;"
      fixChar '"' = "&quot;"
      fixChar c | ord c < 0x80 = [c]
      fixChar c = "&#" ++ show (ord c) ++ ";"
