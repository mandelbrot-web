#include <emscripten.h>
#include <stdlib.h>
#include <string.h>

EM_JS(char *, input, (void), {
  var str = decodeURI(window.location.hash.substring(1));
  var bytes = lengthBytesUTF8(str) + 1;
  var ptr = _malloc(bytes);
  stringToUTF8(str, ptr, bytes);
  return ptr;
})

EM_JS(void, output, (const char* str), {
  document.getElementById("mandelbrot-numerics").innerHTML = UTF8ToString(str);
})

void done(char *out, int out_size, void *arg)
{
  output(out ? out : "failed");
  (void) out_size;
  (void) arg;
}

int main(int argc, char **argv)
{
  static worker_handle numerics = 0;
  if (numerics)
  {
    emscripten_destroy_worker(numerics);
    numerics = 0;
    output("...");
  }
  numerics = emscripten_create_worker("mandelbrot-numerics.js");
  char *in = input();
  int in_size = strlen(in) + 1;
  for (int i = 0; i < in_size; ++i) if (in[i] == ' ') in[i] = 0;
  emscripten_call_worker(numerics, "mandelbrot_numerics", in, in_size, done, 0);
  free(in);
  return 0;
  (void) argc;
  (void) argv;
}
