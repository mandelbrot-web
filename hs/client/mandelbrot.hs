{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main (main) where

import Prelude hiding (Rational)

import GHCJS.Types (JSVal)

import GHCJS.DOM
  ( enableInspector
  , webViewGetDomDocument
  , runWebGUI
  )
import GHCJS.DOM.Window (getLocation, hashChange)
import GHCJS.DOM.Document
  ( Document
  , getHead
  , getBody
  , getElementById
  , createElement
  , createTextNode
  )
import GHCJS.DOM.Element
  ( setInnerHTML
  , submit
  , click
  , getBoundingClientRect
  )
import GHCJS.DOM.ClientRect (getLeft, getTop)
import GHCJS.DOM.Node (appendChild)
import GHCJS.DOM.HashChangeEvent (getNewURL)
import GHCJS.DOM.Location (Location, getHash, setHash)
import GHCJS.DOM.EventM (on, mouseClientXY)
import GHCJS.DOM.HTMLElement
  ( HTMLElement
  , castToHTMLElement
  )
import GHCJS.DOM.HTMLFormElement (castToHTMLFormElement)
import GHCJS.DOM.HTMLInputElement
  ( HTMLInputElement
  , castToHTMLInputElement
  , getValue
  , setValue
  )
import GHCJS.DOM.HTMLTitleElement
  ( HTMLTitleElement
  , castToHTMLTitleElement
  )
import GHCJS.DOM.HTMLCanvasElement
  ( castToHTMLCanvasElement
  , getContext
  )

import Control.Concurrent
  ( MVar
  , newEmptyMVar
  , putMVar
  , takeMVar
  , tryTakeMVar
  , forkIOWithUnmask
  , killThread
  )
import Control.Exception (AsyncException, catch, mask_)
import Control.Monad (forever)
import Control.Monad.Reader (ask)
import Control.Monad.Writer (execWriter, tell)
import Control.Monad.Trans (liftIO)
import Data.Bits (bit)
import Data.Char (ord, chr, isHexDigit, digitToInt)
import Data.Complex (Complex((:+)))
import Data.List (intersperse, nub)
import Data.Maybe (isJust)
import Text.Parsec
  ( choice
  , try
  , getPosition
  , manyTill
  , anyChar
  , string
  , eof
  , setPosition
  , unexpected
  , runParser
  )
import Data.Scientific (toRealFloat)

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , BinaryAngle(..)
  , Block(..)
  , ExternalAngle(..)
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Rational
  , Periods(..)
  , (!)
  , (%)
  , kneading
  , associated
  , rational
  , binary
  , binaryAngle
  , compact
  , addressAngles
  , externalAngles
  , denominator
  , zero
  , one
  , period
  , periods
  , internalAddress
  , angledAddress
  )
import Mandelbrot.Text
  ( Parser
  , Parse(parser)
  , Plain(plain)
  , HTML(html)
  , Glyph(ArrowRight, Star)
  )

data View = View (Complex Double) Double
instance Plain View where
  plain (View (x:+y) r) = show x ++ "+" ++ show y ++ "i@" ++ show r
instance HTML View where
  html (View (x:+y) r)
    = "<span class='m-view'><span class='m-complex'>" ++
      "<span class='m-float'>" ++ show x ++ "</span>" ++
      "<span class='m-plus'>+</span>" ++
      "<span class='m-float'>" ++ show y ++ "</span>" ++
      "<span class='m-i'>i</span></span>" ++
      "<span class='m-at'>@</span>" ++
      "<span class='m-float'>" ++ show r ++ "</span></span>"

data Item
  = BAn BinaryAngle
  | Knd Kneading
  | AAd AngledAddress
  | IAd InternalAddress
  | Rat Rational
  | Per Periods
  | IAn InternalAngle
  | EAn ExternalAngle
  | Vie View

instance Parse View where
  parser = do
    x <- parser
    string "+"
    y <- parser
    string "i"
    string "@"
    r <- parser
    return $ View (toRealFloat x :+ toRealFloat y) (toRealFloat r)

item :: Parser Item
item = choice
  [ t (BAn `fmap` parser) -- starts with ., ends with )
  , t (Knd `fmap` parser) -- starts with 0, 1, *, (, ends with )
  , t (Rat `fmap` parser) -- contains over
  , t (Per `fmap` parser) -- contains p
  , t (Vie `fmap` parser) -- contains +, i and @
  , t (AAd `fmap` parser) -- possibly contains _, over, arrowright
  , t (IAd `fmap` parser) -- possibly contains arrowright, no over
  , do
      p <- getPosition
      s <- manyTill anyChar eof
      setPosition p
      unexpected . show . abbreviate 50 $ s
  ]
  where
    t p = try (do r <- p ; eof ; return r)
    abbreviate n s = case map (splitAt n) (lines s) of
      [(s', [])] -> s'
      (s', _):_ -> s' ++ "..."
      _ -> ""


class Valid t where
  valid :: t -> Bool

instance Valid AngledAddress where
  valid (Unangled 1) = True
  valid (Angled 1 r' a') = go 1 r' a'
    where
      go p r (Unangled pp) =
        zero < r && r < one && p < pp && pp <= p * fromInteger (denominator r)
      go p r (Angled pp r1 a1) =
        zero < r && r < one && p < pp && pp <= p * fromInteger (denominator r) &&
        go pp r1 a1
  valid _ = False

realizable :: AngledAddress -> Bool
realizable a = valid a && all (isJust . addressAngles) (truncations a)

truncations :: AngledAddress -> [AngledAddress]
truncations a' = execWriter $ go (Unangled 1) a'
  where
    go a0 (Unangled _) = tell [a0]
    go a0 (Angled p r (Unangled pp)) = tell [a0,a1,a2]
      where
        a1 = appendAddress a0 r (p * fromInteger (denominator r))
        a2 = appendAddress a0 r pp
    go a0 (Angled p r a3@(Angled pp _ _)) = tell [a0,a1] >> go a2 a3
      where
        a1 = appendAddress a0 r (p * fromInteger (denominator r))
        a2 = appendAddress a0 r pp

class Plain t => Hyper t where
  hyper :: t -> String
  link :: t -> String -> String
  link t s = link' t (stringToHtmlString s)
  link' :: t -> String -> String
  link' t s = "<a href='#!q=" ++ q ++ "' title='" ++ q ++ "'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain t)

class Page t where
  page :: t -> String

instance Page AngledAddress where
  page a0
    | realizable a0 = unlines $
      [ "<table>" ] ++
      map tr (reverse (nub (truncations a0))) ++
      [ "</table>" ]
    | otherwise = ""
    where
      tr a = "<tr><td>" ++ hyper a ++ "</td><td><span class='m-stack'><span>" ++ hyper (binary lo) ++ "</span><span>" ++ hyper (binary hi) ++ "</span></span></td></tr>"
        where
          Just (lo, hi) = addressAngles a

instance Page BinaryAngle where
  page b = unlines $
    [ "<h4>Binary</h4>"
    , link' b (html b)
    , "<h4>Rational</h4>"
    , link' r (html r)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods b))
    , "<h4>Kneading</h4>"
    , link' k (html k)
    , case i' of Just i -> "<h4>Internal Address</h4>\n" ++ link' i (html i) ; Nothing -> ""
    , case a' of Just a -> "<h4>Angled Address</h4>\n" ++ link' a (html a) ; Nothing -> ""
    ] ++ if null (drop 1 es) then [] else
          [ "<h4>Landings</h4>"
          , "<table>" ] ++
          map tr es ++
          [ "</table>" ]
    where
      r = rational b
      k = kneading r
      i' = internalAddress k
      a' = angledAddress r
      es = externalAngles b
      tr e = "<tr" ++ (if b == e then " class='m-strong'" else "") ++ "><td>" ++ hyper e ++ "</td><td>" ++ hyper (rational e) ++ "</td></tr>"

instance Page InternalAngle where
  page i = unlines $
    [ "<h4>Rational</h4>"
    , link' i (html i)
    , "<h4>Bulb</h4>"
    , "<table>" ] ++
    map tr [lo, hi] ++
    [ "</table>"
{-
    , "<h4>Spokes</h4>"
    , "<table>"
    , map tr spokes
    , "</table>"
-}
    ]
    where
      Just (lo, hi) = addressAngles (Angled 1 i (Unangled (fromInteger (denominator i))))
      tr e = "<tr><td>" ++ hyper (binary e) ++ "</td><td>" ++ hyper e ++ "</td></tr>"

instance Page Kneading where
  page k@(StarPeriodic _) = unlines $
    [ "<h4>" ++ html Star ++ "-Periodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ] ++ case associated k of
      Just (u, l) ->
        [ "<h4>Upper</h4>"
        , link' u (html u)
        , "<h4>Lower</h4>"
        , link' l (html l)
        ]
      _ -> []
  page k@(Periodic _) = unlines $
    [ "<h4>Periodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ]
  page k@(PrePeriodic _ _) = unlines $
    [ "<h4>Preperiodic</h4>"
    , link' k (html k)
    , "<h4>Periods</h4>"
    , hyper (Periods (periods k))
    ]

instance Page Periods where
  page (Periods (pp, p)) = unlines $
    [ "<h4>Binary Angles</h4>"
    , "<table>" ] ++
    [ "<tr><td>" ++ hyper (BinaryAngle bb b) ++ "</td></tr>"
    | ii <- [ 0 .. bit pp - 1 ], let bb = Block ii pp
    , i <- [ 0 .. bit p - 1 ], let b = Block i p
    , binaryAngle bb b == BinaryAngle bb b
    ] ++ [ "</table>" ] ++
    (if pp == 0 then
      [ "<h4>" ++ html Star ++ "-Periodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (StarPeriodic b) ++ "</td></tr>"
      | i <- [ 0 .. bit (p - 1) - 1 ], let b = Block i (p - 1), b ! 0
      ] ++ [ "</table>"
      , "<h4>Periodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (Periodic b) ++ "</td></tr>"
      | i <- [ 0 .. bit p - 1 ], let b = Block i p, b ! 0, b == compact b
      ] ++ [ "</table>" ]
    else
      [ "<h4>Preperiodic Kneading Sequences</h4>"
      , "<table>" ] ++
      [ "<tr><td>" ++ hyper (PrePeriodic bb b) ++ "</td></tr>"
      | ii <- [ 0 .. bit pp - 1 ], let bb = Block ii pp, bb ! 0
      , i <- [ 0 .. bit p - 1 ], let b = Block i p
      , binaryAngle bb b == BinaryAngle bb b -- fixme need generalized version
      ] ++ [ "</table>" ])

instance Page View where
  page _ = unlines $
    [ "<canvas id='canvas' width='1024' height='576'></canvas>" ]

instance Hyper AngledAddress where
  hyper a
    | realizable a = "<span class='m-angledaddress'>" ++ hyper' (Unangled 1) a ++ "</span>"
    | otherwise = "<span class='m-angledaddress'>" ++ stringToHtmlString (plain a) ++ "</span>"
    where
    hyper' a0 (Unangled p) = link a0 . plain $ p
    hyper' a0 (Angled p r (Unangled pp))
      =  link a0 (plain p)
      ++ "<sub>" ++ link' r (html r) ++ "</sub>" ++ html ArrowRight
      ++ link a2 (plain pp)
      where
        a2 = appendAddress a0 r pp
    hyper' a0 (Angled p r a3@(Angled pp _ _))
      =  link a0 (plain p)
      ++ "<sub>" ++ link' r (html r) ++ "</sub>" ++ html ArrowRight
      ++ hyper' a2 a3
      where
        a2 = appendAddress a0 r pp

instance Hyper InternalAddress where
  hyper (InternalAddress (1:a)) = "<span class='m-internaladdress'>" ++ hyper' (InternalAddress [1]) a ++ "</span>"
    where
      hyper' a0 [] = link a0 . plain . period $ a0
      hyper' a0 (p:ps) =
        (link a0 . plain . period $ a0) ++
        html ArrowRight ++
        hyper' (appendInternal a0 p) ps
  hyper i = "<span class='m-internaladdress'>" ++ stringToHtmlString (plain i) ++ "</span>"

appendInternal :: InternalAddress -> Int -> InternalAddress
appendInternal (InternalAddress ps) p = InternalAddress (ps ++ [p])

instance Hyper InternalAngle where
  hyper i = link' i (html i)
  link' i s = "<a href='#!q=" ++ q ++ "&amp;t=internal-angle' title='" ++ q ++ " (internal angle)'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain i)
instance Hyper ExternalAngle where
  hyper e = link' e (html e)
  link' e s = "<a href='#!q=" ++ q ++ "&amp;t=external-angle' title='" ++ q ++ " (external angle)'>" ++ s ++ "</a>"
    where q = stringToHtmlString (plain e)
instance Hyper BinaryAngle where hyper b = link' b (html b)
instance Hyper Kneading where hyper k = link' k (html k)
instance Hyper Periods where hyper p = link' p (html p)
instance Hyper View where hyper v = link' v (html v)

appendAddress :: AngledAddress -> InternalAngle -> Int -> AngledAddress
appendAddress (Unangled p0) r p = Angled p0 r (Unangled p)
appendAddress (Angled p0 r0 a0) r p = Angled p0 r0 (appendAddress a0 r p)

maxPeriodList :: Int
maxPeriodList = 10

describe :: Item -> Either String (String, String, String)
describe (BAn b)
  | zero <= r && r <= one = Right (plain b', html b', "<h3>External Angle</h3>\n" ++ page b')
  | otherwise = Left "invalid binary angle"
  where
    r = rational b
    b' = binary r
describe (EAn r)
  | zero <= r && r <= one = Right (plain r, html r, "<h3>External Angle</h3>\n" ++ page (binary r))
  | otherwise = Left "invalid external angle"
describe (IAn i)
  | zero < i && i < one = Right (plain i, html i, "<h3>Internal Angle</h3>\n" ++ page i)
  | otherwise = Left "invalid internal angle"
describe (Knd k)
  | otherwise = Right (plain k, html k, "<h3>Kneading Sequence</h3>\n" ++ page k)
describe (Rat r) = Right (plain r, html r, "<h3>Rational</h3>\n" ++ if zero <= r && r <= one then "<h4>External Angle</h4>\n" ++ link' e (html e) ++ "\n" ++ if zero < r && r < one then "<h4>Internal Angle</h4>\n" ++ link' i (html i) ++ "\n" else "" else "")
  where
    i = InternalAngle r
    e = ExternalAngle r
describe (AAd a)
  | realizable a = Right (plain a, html a, "<h3>Angled Address</h3>\n" ++ page a)
  | otherwise = Left "invalid angled address"
describe (IAd a)
  | otherwise = Right (plain a, html a, unlines $ [ plain a ])
describe (Per ps@(Periods (pp, p)))
  | pp + p > maxPeriodList = Left $ "periods too large, period limit " ++ show maxPeriodList
  | pp >= 0 && p >= 1 = Right (plain ps, html ps, "<h3>Periods</h3>\n" ++ page ps)
  | otherwise = Left "invalid periods"
describe (Vie v) = Right (plain v, html v, "<h3>View</h3>" ++ page v)

help :: [String]
help = [ "<h2>Examples</h2>" ] ++ intersperse ", "
  [ hyper (BinaryAngle (Block 0 0) (Block 1 2))
  , hyper (BinaryAngle (Block 2 2) (Block 1 2))
  , hyper (2%5 :: ExternalAngle)
  , hyper (2%5 :: InternalAngle)
  , hyper (StarPeriodic (Block 2 2))
  , hyper (Periodic (Block 2 2))
  , hyper (PrePeriodic (Block 1 2) (Block 2 2))
  , link' a (html a)
  , link' i (html i)
  , hyper (Periods (0, 4))
  , hyper (Periods (2, 2))
  , hyper (View ((-0.75) :+ 0) 1.25)
  ]
  where
    a = Angled 1 (1%2) (Angled 2 (1%3) (Unangled 5))
    i = InternalAddress [1,2,5]

worker :: Location -> Document -> HTMLInputElement -> HTMLTitleElement -> HTMLElement -> MVar String -> (forall a. IO a -> IO a) -> IO ()
worker loc doc input titleNode output mv unmask =
  forever $ unmask update `catch` \(_e::AsyncException) -> return ()
    where
      update = do
        url  <- takeMVar mv
        setInnerHTML output (Just $ "<span class='busy'>...calculating...</span>")
        case dropWhile (/= '#') (percentDecode url) of
          '#':'!':s -> case fmap (runParser item () "") (lookup "q" (parseAnchor s)) of
            Just (Right i) -> case describe (case i of
                  Rat r -> case lookup "t" (parseAnchor s) of
                    Just "internal-angle"
                      | zero < r && r < one -> IAn (InternalAngle r)
                    Just "external-angle"
                      | zero <= r && r <= one -> EAn (ExternalAngle r)
                    _ -> i
                  _ -> i) of
              Left e -> setInnerHTML output (Just $ "<span class='error'>" ++ stringToHtmlString (show e) ++ "</span>" ++ unlines help)
              Right (ptitle, title, content) -> do
                setValue input (lookup "q" (parseAnchor s))
                setInnerHTML titleNode (Just $ stringToHtmlString ptitle)
                setInnerHTML output (Just $ "<h2>" ++ title ++ "</h2>\n" ++ content ++ "\n")
                case i of
                  Vie (View (cx:+cy) r) -> do
                    Just canvas <- fmap castToHTMLCanvasElement <$> getElementById doc "canvas"
                    context <- getContext canvas "2d"
                    js_m_d_render context 1024 576 cx cy r 25 100000
                    on canvas click $ do
                      Just rect <- getBoundingClientRect canvas
                      rx <- realToFrac <$> getLeft rect
                      ry <- realToFrac <$> getTop rect
                      (ix, iy) <- mouseClientXY
                      let dx = 2 * ((fromIntegral ix + 0.5 - rx) / 1024 - 0.5) * (1024 / 576)
                          dy = 2 * (0.5 - (fromIntegral iy + 0.5 - ry) / 576)
                          ddx = r * dx / 2
                          ddy = r * dy / 2
                          cx' = cx + ddx
                          cy' = cy + ddy
                          r' = r / 2
                      setHash loc $ "#!q=" ++ plain (View (cx' :+ cy') r')
                      return ()
                    return ()
                  _ -> return ()
            Just (Left e) -> setInnerHTML output (Just $ "<span class='error'>parse failure: " ++ stringToHtmlString (show e) ++ "</span>" ++ unlines help)
            _ ->  setInnerHTML output (Just $ unlines help)
          _ -> setInnerHTML output (Just $ unlines help)

main :: IO ()
main = runWebGUI $ \window -> do
  enableInspector window
  Just loc <- getLocation window
  Just doc <- webViewGetDomDocument window
  Just head' <- getHead doc
  Just titleNode <- fmap castToHTMLTitleElement <$> createElement doc (Just "title")
  ttext <- createTextNode doc $ "mandelbrot-web"
  appendChild titleNode ttext
  appendChild head' (Just titleNode)
  Just style <- createElement doc (Just "style")
  text <- createTextNode doc $ unlines css
  appendChild style text
  appendChild head' (Just style)
  Just body <- getBody doc
  setInnerHTML body (Just $ "<h1>mandelbrot-web</h1><form id='form'><input id='input' size='60' /><button type='submit'>Submit</button></form><div id='output'>" ++ unlines help ++ "</div>")
  Just form   <- fmap castToHTMLFormElement  <$> getElementById doc "form"
  Just input  <- fmap castToHTMLInputElement <$> getElementById doc "input"
  Just output <- fmap castToHTMLElement      <$> getElementById doc "output"
  mv <- newEmptyMVar
  workerThread <- mask_ $ forkIOWithUnmask (worker loc doc input titleNode output mv)
  on window hashChange $ do
    url <- getNewURL =<< ask
    liftIO $ do
      killThread workerThread
      tryTakeMVar mv -- discard existing
      putMVar mv url
    return ()
  on form submit $ do
    Just s <- getValue input
    setHash loc $ "#!q=" ++ s
    return ()
  url <- getHash loc
  putMVar mv url
  return ()

parseAnchor :: String -> [(String, String)]
parseAnchor "" = []
parseAnchor s = case break (== '&') s of
  (q, r) -> case break (== '=') q of
    (a, b) -> (a, drop 1 b) : parseAnchor (drop 1 r)

css :: [String]
css =
  [ "body { text-align: center; }"
  , "body > div#output { display: inline-block; margin: auto; }"
  , "body > div#output > dl { display: inline-block; margin: auto }"
  , "body > div#output > table { display: inline-block; margin: auto }"
  , "body > div#output > * > * { text-align: left; }"
  , "a { text-decoration: none; }"
  , "dt { font-weight: bold; }"
  , ".m-stack { display: inline-block; text-align: right; vertical-align: middle; }"
  , ".m-stack > * { display: block; }"
  , ".m-strong { font-weight: bold; }"
  , ".m-binaryangle, .m-kneading, .m-view { font-family: monospace; }"
  , ".m-binaryangle .m-periodic, .m-kneading .m-periodic { border-top: 1px solid; }"
  , ".m-rational { display: inline-block; text-align: right; vertical-align: middle; font-size: 75%; }"
  , ".m-rational .m-over { display: none; }"
  , ".m-rational .m-denominator { display: block; border-top: 1px solid; }"
  , ".m-angledaddress .m-arrowright, .m-internaladdress .m-arrowright { margin-left: 5px; margin-right: 5px; }"
  , ".m-complex .m-plus, .m-complex .m-i, .m-view .m-at { font-weight: bold; }"
  , ".error { background-color: #ffdddd; }"
  , ".busy { background-color: #ffeedd; }"
  , "canvas { border: 1px solid; }"
  ]

-- | Processing Strings into Html friendly things.
stringToHtmlString :: String -> String
stringToHtmlString = concatMap fixChar
    where
      fixChar '<' = "&lt;"
      fixChar '>' = "&gt;"
      fixChar '&' = "&amp;"
      fixChar '"' = "&quot;"
      fixChar c | ord c < 0x80 = [c]
      fixChar c = "&#" ++ show (ord c) ++ ";"

-- from happstack-server 7.4.6.1
percentDecode :: String -> String
percentDecode [] = ""
percentDecode ('%':x1:x2:s) | isHexDigit x1 && isHexDigit x2 =
    chr (digitToInt x1 * 16 + digitToInt x2) : percentDecode s
percentDecode (c:s) = c : percentDecode s

foreign import javascript "m_d_render($1,$2,$3,$4,$5,$6,$7,$8);" js_m_d_render :: JSVal -> Int -> Int -> Double -> Double -> Double -> Double -> Int -> IO ()
